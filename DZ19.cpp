﻿#include <iostream>

class Animal
{
public:
    virtual void makeSound() const = 0;
};

class Dog : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Moo!\n";
    }
};

class Duck : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Quack!\n";
    }
};

class Lion : public Animal
{
public:
    void makeSound() const override
    {
        std::cout << "Roar!\n";
    }
};

int main()
{
    Animal* animals[5];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Cow();
    animals[3] = new Duck();
    animals[4] = new Lion();

    for (Animal* a : animals)
        a->makeSound();
}